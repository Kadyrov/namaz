﻿namespace Namaz.Tests
{
    using System.Collections.Generic;

    using Domain;
    using Model.Dto;

    using Model;

    using Xunit;

    public class ScheduleTests
    {
        private UserRepo _repo;

        public ScheduleTests()
        {
            _repo = new UserRepo();

            CityRepo.Populate();
        }

        [Fact]
        public void Create_Schedule()
        {
            var dto = new UserDto
            {
                Email = "test@test.com",
                Name = "Phil",
                Password = "123"
            };
            
            using (var db = new NamazDbContext())
            {
                _repo.Create(dto);

                new ScheduleRepo().Create(dto, new ScheduleDto
                                                   {
                                                       Month = 1,
                                                       Days = new List<PrayDay>
                                                                  {
                                                                      new PrayDay{Asr = new PrayTime{Name = "1", Time="123123"}},
                                                                      new PrayDay{Isha = new PrayTime{Name = "1", Time="123123"}},
                                                                      new PrayDay{Magrib = new PrayTime{Name = "1", Time="123123"}},
                                                                      new PrayDay{Zuhr = new PrayTime{Name = "1", Time="123123"}},
                                                                      new PrayDay{Voshod = new PrayTime{Name = "1", Time="123123"}}
                                                                  },
                                                        CityId = "74248"
                                                   });
                
                db.SaveChanges();

                db.Database.ExecuteSqlCommand("truncate public.schedule cascade");
                db.Database.ExecuteSqlCommand("truncate public.user cascade");

                db.SaveChanges();
            }
        } 
    }
}