﻿namespace Namaz.Domain
{
    using System.Security.Cryptography;
    using System.Text;
    using Model;

    public class NamazHasher
    {
        private MD5 _hash = MD5.Create();

        public string CreateHash(ISchedule schedule)
        {
            if (schedule == null || schedule.Days == null || schedule.Days.Count == 0)
                return string.Empty;

            var builder = new StringBuilder();

            builder.Append(schedule.Month + "_");

            foreach (var prayDay in schedule.Days)
            {
                builder.Append(prayDay.DayOfMonth)
                    .Append(prayDay.Asr)
                    .Append(prayDay.Fadzhr)
                    .Append(prayDay.Isha)
                    .Append(prayDay.Magrib)
                    .Append(prayDay.Voshod)
                    .Append(prayDay.Zuhr);
            }

            return GetMd5Hash(_hash, builder);
        }

        static string GetMd5Hash(MD5 md5Hash, StringBuilder builder)
        {
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(builder.ToString()));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}