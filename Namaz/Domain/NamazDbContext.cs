﻿namespace Namaz.Domain
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Migrations;
    using Model;

    public class NamazDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<CitiesHolder> CitiesHolders { get; set; }
        public DbSet<AuthProvider> AuthProviders { get; set; } 

        public NamazDbContext() : base("Namaz")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<NamazDbContext, Configuration>());
            Database.Initialize(false);
            //Database.SetInitializer(new NullDatabaseInitializer<NamazDbContext>());
        }

        public override int SaveChanges()
        {
            foreach (
                var entry in
                    ChangeTracker.Entries<BaseEntity>()
                        .Where(x => x.State == EntityState.Added || x.State == EntityState.Modified))
            {
                var now = DateTime.UtcNow;
                var state = entry.State;

                var entity = entry.Entity;
                entity.EditDate = now;

                switch (state)
                {
                    case EntityState.Added:
                        entity.CreateDate = now;
                        break;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder mb)
        {
            mb.Entity<User>().ToTable("user", "public")
                .HasOptional(x => x.Schedule)
                .WithMany()
                .Map(x => x.MapKey("schedule_id"));

            mb.Entity<Schedule>().ToTable("schedule", "public")
                .HasRequired(x => x.Creator)
                .WithMany()
                .Map(x => x.MapKey("user_id"));
            mb.Entity<Schedule>()
                .HasRequired(x => x.City)
                .WithMany()
                .Map(x => x.MapKey("city_id"));
            mb.Entity<Schedule>()
                .HasOptional(x => x.Next)
                .WithMany()
                .Map(x => x.MapKey("next_id"));

            mb.Entity<City>().ToTable("city", "public")
                .HasRequired(x => x.Holder)
                .WithMany(x => x.Cities)
                .Map(x => x.MapKey("holder_id"));

            mb.Entity<CitiesHolder>().ToTable("cities_holder", "public")
                .Ignore(x => x.Regions)
                .HasMany(x => x.Cities)
                .WithRequired(x => x.Holder);

            mb.Entity<AuthProvider>().ToTable("auth_provider", "public")
                .HasRequired(x => x.User)
                .WithMany()
                .Map(x => x.MapKey("user_id"));

            base.OnModelCreating(mb);
        }
    }
}