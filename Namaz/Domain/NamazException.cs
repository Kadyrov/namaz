﻿using System;

namespace Namaz.Domain
{
    public class NamazException : Exception
    {
        public NamazException(string code) : base(code)
        {
        }
    }
}