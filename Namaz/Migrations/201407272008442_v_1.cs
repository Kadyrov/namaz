namespace Namaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v_1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.city",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        name_en = c.String(maxLength: 1073741823, fixedLength: true),
                        ext_id = c.String(maxLength: 1073741823, fixedLength: true),
                        country_ext_id = c.String(maxLength: 1073741823, fixedLength: true),
                        lat = c.String(maxLength: 1073741823, fixedLength: true),
                        lng = c.String(maxLength: 1073741823, fixedLength: true),
                        region_ext_id = c.String(maxLength: 1073741823, fixedLength: true),
                        holder_id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("public.cities_holder", t => t.holder_id, cascadeDelete: true)
                .Index(t => t.holder_id)
                .Index(t => t.ext_id)
                .Index(t => t.region_ext_id);
            
            CreateTable(
                "public.cities_holder",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        tz = c.String(maxLength: 1073741823, fixedLength: true),
                        name_en = c.String(maxLength: 1073741823, fixedLength: true),
                        ext_id = c.String(maxLength: 1073741823, fixedLength: true),
                        name_ru = c.String(maxLength: 1073741823, fixedLength: true),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.ext_id);
            
            CreateTable(
                "public.schedule",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        month = c.Int(nullable: false),
                        region_code = c.String(maxLength: 1073741823, fixedLength: true),
                        counter = c.Int(nullable: false),
                        hash = c.String(maxLength: 1073741823, fixedLength: true),
                        days = c.Binary(),
                        user_id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("public.user", t => t.user_id, cascadeDelete: true)
                .Index(t => t.user_id);
            
            CreateTable(
                "public.user",
                c => new
                    {
                        id = c.Long(nullable: false, identity: true),
                        name = c.String(maxLength: 1073741823, fixedLength: true),
                        email = c.String(maxLength: 1073741823, fixedLength: true),
                        password = c.String(maxLength: 1073741823, fixedLength: true),
                    })
                .PrimaryKey(t => t.id);

            CreateTable(
                "public.auth_provider",
                c => new
                {
                    id = c.Long(nullable: false, identity: true),
                    code = c.String(fixedLength: true),
                    cur_token = c.String(fixedLength: true),
                    old_token = c.String(fixedLength: true),
                    last_check = c.DateTime(nullable: false),
                    user_id = c.Long(nullable: false),
                })
                .PrimaryKey(t => t.id)
                .ForeignKey("public.user", t => t.user_id, cascadeDelete: true)
                .Index(t => t.user_id);
        }
        
        public override void Down()
        {

            DropForeignKey("public.auth_provider", "user_id", "public.user");
            DropIndex("public.auth_provider", new[] { "user_id" });
            DropTable("public.auth_provider");
            DropForeignKey("public.schedule", "user_id", "public.user");
            DropForeignKey("public.city", "holder_id", "public.cities_holder");
            DropIndex("public.schedule", new[] { "user_id" });
            DropIndex("public.city", new[] { "holder_id" });
            DropTable("public.user");
            DropTable("public.schedule");
            DropTable("public.cities_holder");
            DropTable("public.city");
        }
    }
}
