namespace Namaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2014101500 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.schedule", "next_id", b => b.Long( true));
            CreateIndex("public.schedule", "next_id");
            AddForeignKey("public.schedule", "next_id", "public.schedule", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("public.schedule", "next_id", "public.schedule");
            DropIndex("public.schedule", new[] { "next_id" });
            DropColumn("public.schedule", "next_id");
        }
    }
}
