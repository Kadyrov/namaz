namespace Namaz.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.user", "schedule_id", builder => builder.Long());
            AddForeignKey("public.user", "schedule_id", "public.schedule", "id", name: "fk_user_schedule");
            CreateIndex("public.user", "schedule_id", name: "ind_user_schedule");
        }
        
        public override void Down()
        {
            DropIndex("public.user", "ind_user_schedule");
            DropForeignKey("public.user", "fk_user_schedule");
            DropColumn("public.user", "schedule_id");
        }
    }
}
