﻿namespace Namaz.Controllers
{
    using System;
    using System.Web.Mvc;
    using Domain;
    using Model.Dto;

    public class BaseController : Controller
    {
        public ActionResult Success(string message = null, object data = null)
        {
            return Json(new {success = true, data}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Fail(string message, object data = null)
        {
            return Json(new { success = false, message, data}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Wrapped(ResultDto result)
        {
            if (result.Success)
                return Success(result.Message, result.Data);

            return Fail(result.Message, result.Data);
        }

        public ActionResult InTryCatch(Func<ResultDto> func)
        {
            try
            {
                return Wrapped(func());
            }
            catch (NamazException e)
            {
                return Json(new {success = false, error = e.Message}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Fail(e.ToString());
            }
        }
    }
}