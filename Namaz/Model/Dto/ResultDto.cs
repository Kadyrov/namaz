﻿namespace Namaz.Model.Dto
{
    public class ResultDto
    {
        public string Message { get; set; }

        public bool Success { get; set; }

        public object Data { get; set; }

        public static ResultDto Succeded(object data = null)
        {
            return new ResultDto()
            {
                Success = true,
                Data = data
            };
        }

        public static ResultDto Failed(string message)
        {
            return new ResultDto {Message = message};
        }
    }
}