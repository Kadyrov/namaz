﻿namespace Namaz.Model
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class User : BaseEntity
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("password")]
        public string Password { get; set; }

        [Column("salt")]
        public string Salt { get; set; }

        [Column("schedule_id")]
        public Schedule Schedule { get; set; }
    }
}