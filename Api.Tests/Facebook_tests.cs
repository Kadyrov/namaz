﻿using System.Linq;
using Api.Domain;
using Facebook;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Tests
{
    public class FacebookTests
    {
        [Fact]
        void Test_Me()
        {
            var appSecret = "4ed978dc354e0ba4906f0920d1ac54fa";
            var accessToken =
                "CAACEdEose0cBAALipV5hZB1wPAsNYUq5HONKRzxzPU8vWpihIfMyrySB3cPUL82d0bCcYMvZBNB3lNevJKMZCVOBhKQigZCTzfzrxujuAzEebs4gfHqmwViwURJ9pHrsW7i4ZCzBIUG8j649BIZAL4cP4c99BmCkFslV9BO0ARdMbCXDxboJFr79fjXZC8n1ZCdEDachILCviW2vwqroHzo9ZA8honbO9BD0ZD";

            var cl = new FacebookClient(accessToken);
            var me = cl.Get("me");
        }

        [Fact]
        void Populate_Email()
        {
            using (var ctx = new NamazDbContext())
            {
                var providers = ctx.AuthProviders.Include(provider => provider.User).ToList();

                foreach (var prov in providers)
                {
                    try
                    {
                        var cl = new FacebookClient(prov.CurrentToken);
                        dynamic me = cl.Get("me");

                        var email = me.email;

                        prov.User.Email = email;

                        ctx.Users.Attach(prov.User);
                    }
                    catch { }
                }

                ctx.SaveChanges();
            }
        }
    }
}
