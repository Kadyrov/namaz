﻿using Api.Domain;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return Ok("Ok");
        }

        public ActionResult PopulateCities([FromServices] IWebHostEnvironment env)
        {
            CityRepo.Populate(env);

            return null;
        }
    }
}