﻿using System;
using System.Linq;
using Api.Domain;
using Api.Model.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers
{
    public class StatsController : BaseController
    {
        private StatRepo _statRepo;

        public StatsController()
        {
            _statRepo = new StatRepo();
        }

        public ActionResult Counters(string date)
        {
            ResultDto data = _statRepo.GetCounters(date);

            return Wrapped(data);
        }
    }

    public class StatRepo
    {
        public ResultDto GetCounters(string date)
        {
            DateTime datetime;
            var parsed = DateTime.TryParse(date, out datetime);

            using (var db = new NamazDbContext())
            {
                var userCount = db.Users.WhereIf(parsed, x => x.CreateDate.Date == datetime.Date).Count();

                var schedules = db.Schedules
                    .Include(x => x.Creator)
                    .Include(x => x.City)
                    .WhereIf(parsed, x => x.CreateDate.Date == datetime.Date)
                    .Select(x => new
                    {
                        cityId = x.City.ExtId,
                        regionId = x.City.RegionExtId,
                        countryId = x.City.CountryExtId,
                        authorName = x.Creator.Name,
                        authorId = x.Creator.Id,
                        source = x.Source,
                        rate = x.Counter,
                        scheduleId = x.Id,
                        month = x.Month,
                        year = x.Year
                    }).ToList();

                return ResultDto.Succeded(new {userCount, schedules});
            }
        }
    }
}