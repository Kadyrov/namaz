﻿using Api.Domain;
using Api.Model.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class ScheduleController : BaseController
    {
        [HttpGet]
        public ActionResult Create(UserDto userDto, ScheduleDto scheduleDto)
        {
            return InTryCatch(() => new ScheduleRepo().Create(userDto, scheduleDto));
        }

        [HttpGet]
        public ActionResult Edit(UserDto userDto, ScheduleDto scheduleDto,
            long scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().Edit(userDto, scheduleDto, scheduleId));
        }

        [HttpGet]
        public ActionResult CreateIfExists(UserDto userDto, ScheduleDto scheduleDto)
        {
            return InTryCatch(() => new ScheduleRepo().CreateIfExists(userDto, scheduleDto));
        }

        [HttpGet]
        public ActionResult Apply(UserDto userDto,
            int scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().Apply(userDto, scheduleId));
        }

        [HttpGet]
        public ActionResult Get(int id)
        {
            return InTryCatch(() => new ScheduleRepo().Get(id));
        }

        public ActionResult IncreaseRate(long scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().IncreaseRate(scheduleId));
        }

        public ActionResult DecreaseRate(long scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().DecreaseRate(scheduleId));
        }

        [HttpGet]
        public ActionResult GetSchedulesByUser(UserDto userDto)
        {
            return InTryCatch(() => new ScheduleRepo().GetSchedulesByUser(userDto));
        }

        public ActionResult GetSchedulesByCity(string cityId)
        {
            return InTryCatch(() => new ScheduleRepo().GetSchedulesByCity(cityId));
        }

        public ActionResult GetNext(long scheduleId)
        {
            return InTryCatch(() => new ScheduleRepo().GetNext(scheduleId));
        }
    }
}