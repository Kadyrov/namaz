﻿using System;
using Api.Domain;
using Api.Model.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class BaseController : Controller
    {
        public ActionResult Success(string message = null, object data = null)
        {
            return Json(new {success = true, data});
        }

        public ActionResult Fail(string message, object data = null)
        {
            return Json(new { success = false, message, data});
        }

        public ActionResult Wrapped(ResultDto result)
        {
            if (result.Success)
                return Success(result.Message, result.Data);

            return Fail(result.Message, result.Data);
        }

        public ActionResult InTryCatch(Func<ResultDto> func)
        {
            try
            {
                return Wrapped(func());
            }
            catch (NamazException e)
            {
                return Json(new {success = false, error = e.Message});
            }
            catch (Exception e)
            {
                return Fail(e.ToString());
            }
        }
    }
}