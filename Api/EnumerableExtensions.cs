﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Api
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<TEntity> WhereIf<TEntity>(this IEnumerable<TEntity> data,
                                                            bool predicate,
                                                            Func<TEntity, bool> queryPredicate)
        {
            return predicate ? data.Where(queryPredicate) : data;
        }
    }
}