﻿using Api.Model;
using Api.Model.Dto;
using AutoMapper;

namespace Api
{
    public static class Mapper
    {
        static Mapper()
        {
            var config = new MapperConfiguration(m =>
            {
                m.CreateMap<ScheduleDto, Schedule>()
                    .ForMember(s => s.CityId, d => d.Ignore())
                    .ReverseMap();
            });
            Instance = config.CreateMapper();
        }

        public static IMapper Instance { get; }
    }
}