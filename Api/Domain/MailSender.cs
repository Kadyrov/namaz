﻿using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using Api.Model;

namespace Api.Domain
{
    public class MailSender
    {
        private readonly NamazDbContext _ctx;

        private Dictionary<int, string> _months = new Dictionary<int, string>
        {
            {1, "январь"},
            {2, "февраль"},
            {3, "март"},
            {4, "апрель"},
            {5, "май"},
            {6, "июнь"},
            {7, "июль"},
            {8, "август"},
            {9, "сентябрь"},
            {10, "октябрь"},
            {11, "ноябрь"},
            {12, "декабрь"},
        };

        public MailSender(NamazDbContext ctx)
        {
            _ctx = ctx;
        }

        public void SendMessage(User user, string language, string month1, string month2)
        {
            if (user == null || string.IsNullOrEmpty(user.Email))
            {
                return;
            }

            string text = GetText(user, language, GetMonth(month1), GetMonth(month2));
            string subject = GetSubject(language);

            Send(user, subject, text);
        }

        private static void Send(User user, string subject, string text)
        {
            var client = new SmtpClient("smtp.yandex.ru");
            client.Credentials = new NetworkCredential("mail@salattime.net", "namaz!Q@W#E");
            client.Port = 25;
            client.EnableSsl = true;
            client.SendCompleted += (sender, args) => { };

            var msg = new MailMessage("mail@salattime.net",
                user.Email,
                subject,
                text);

            client.SendAsync(msg, user);
        }

        private string GetMonth(string monthNum)
        {
            int month;
            if (!int.TryParse(monthNum, out month))
            {
                throw new NamazException(ErrorCodes.Common.ArgumentError);
            }

            string monthName;
            if (!_months.TryGetValue(month, out monthName))
            {
                throw new NamazException(ErrorCodes.Common.ArgumentError);
            }

            return monthName;
        }

        private string GetSubject(string language)
        {
            return "Расписание";
        }

        private string GetText(User user, string language, string month1, string month2)
        {
            return string.Format(@"Уважаемый {0}!

Вы создали расписание намаза на {1} в приложении Salat Time. Это расписание уже используют другие пользователи приложения. Сейчас конец месяца, поэтому мы просим Вас создать расписание на следующий месяц — {2}. Иначе пользователи Вашего расписания увидят лишь пустой экран.
(Если Вы уже создали расписание на {2}, ничего делать не надо.)

Несколько советов по редактированию расписания:
1) В поле Источник не пишите слова ""мечеть"", ""сайт"", Ваше имя и почту. Просим указывать название сайта (напр., umma.ru), мечети (напр., мечеть ""Кул Шариф"") или короткое название другого источника. Важно указать правильный источник, так как, исходя из него, пользователи выбирают расписание.

2) В названии сайта не стоит указывать полную ссылку (напр, ""http://umma.ru/kalendar/month/11/year/2014""), она не влезет в экран приложения. 

3) При создании расписания внимательно проверьте время на каждый день. Приложение не берет расписание автоматически из указанного источника. Расписание создаете Вы.

Спасибо, что пользуетесь нашим приложением.
Команда Salat Time.", user.Name, month1, month2);
        }

        public void SendMessage(User user, string message)
        {
            var msg = string.Format("Уважаемый {0}!\r\n\r\n{1}", user.Name, message);
            Send(user, "Сообщение от Salat Time", msg);
        }
    }
}