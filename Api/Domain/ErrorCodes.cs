﻿namespace Api.Domain
{
    public class ErrorCodes
    {
        public class Common
        {
            public const string ArgumentError = "001";
        }

        public class UserErrors
        {
            public const string Unknown = "100";
            public const string ParametersAreEmpty = "100.1";

            public const string EmailIsEmpty = "101";
            public const string NameIsEmpty = "102";

            public const string PasswordIsEmpty = "103.1";
            public const string PasswordIsInvalid = "103.2";

            public const string Exists = "104";
            public const string NotExists = "105";

            public const string FieldNotFound = "106";

            public const string TokenIsInvalid = "107.1";
            public const string TokenHasExpired = "107.2";
        }

        public class ScheduleErrors
        {
            public const string Unknown = "200";
            public const string ParametersAreEmpty = "200.1";

            public const string CityNotFound = "201";

            public const string ExistsForUserCityAndMonth = "202";

            public const string NotFoundById = "203";

            public const string NoNextSchedule = "204";
        }
    }
}