﻿using System;
using System.Linq;
using Api.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Api.Domain
{
    public class NamazDbContext : DbContext
    {
        public static readonly ILoggerFactory MyLoggerFactory
            = LoggerFactory.Create(builder =>
            {
                builder.AddFilter(l => l == LogLevel.Information).AddDebug().AddConsole();
            });

        public DbSet<User> Users { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<CitiesHolder> CitiesHolders { get; set; }
        public DbSet<AuthProvider> AuthProviders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLoggerFactory(MyLoggerFactory)
                .UseNpgsql("Server=localhost;Port=5432;Database=namaz;User Id=namaz;Password=namaz;",
                builder => builder.EnableRetryOnFailure().SetPostgresVersion(9, 3));
        }

        public override int SaveChanges()
        {
            foreach (
                var entry in
                    ChangeTracker.Entries<BaseEntity>()
                        .Where(x => x.State == EntityState.Added || x.State == EntityState.Modified))
            {
                var now = DateTime.UtcNow;
                var state = entry.State;

                var entity = entry.Entity;
                entity.EditDate = now;

                switch (state)
                {
                    case EntityState.Added:
                        entity.CreateDate = now;
                        break;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder mb)
        {
            mb.Entity<User>().ToTable("user", "public")
                .HasOne(x => x.Schedule)
                .WithMany();

            mb.Entity<Schedule>().ToTable("schedule", "public")
                .HasOne(x => x.Creator)
                .WithMany()
                .IsRequired(true);

            mb.Entity<Schedule>()
                .HasOne(x => x.City)
                .WithMany()
                .IsRequired();

            mb.Entity<Schedule>()
                .HasOne(x => x.Next)
                .WithMany();

            mb.Entity<City>().ToTable("city", "public")
                .HasOne(x => x.Holder)
                .WithMany(x => x.Cities)
                .IsRequired();

            mb.Entity<CitiesHolder>().ToTable("cities_holder", "public")
                .Ignore(x => x.Regions)
                .HasMany(x => x.Cities)
                .WithOne(x => x.Holder)
                .IsRequired();

            mb.Entity<AuthProvider>().ToTable("auth_provider", "public")
                .HasOne(x => x.User)
                .WithMany()
                .IsRequired();
        }
    }
}