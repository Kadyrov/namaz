﻿using System;

namespace Api.Domain
{
    public class NamazException : Exception
    {
        public NamazException(string code) : base(code)
        {
        }
    }
}