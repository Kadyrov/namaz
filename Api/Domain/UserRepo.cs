﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using Api.Model;
using Api.Model.Dto;
using Facebook;
using FastMember;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Crypto.Digests;
using MailMessage = System.Net.Mail.MailMessage;

namespace Api.Domain
{
    public class UserRepo
    {
        private int _tokenExp = 10;

        public bool Exists(string login)
        {
            using (var db = new NamazDbContext())
            {
                return db.Users.Any(x => x.Email.ToLower() == login.ToLower());
            }
        }

        public ResultDto CheckUser(string login, string password)
        {
            using (var db = new NamazDbContext())
            {
                var user = db.Users.SingleOrDefault(x => x.Email.ToLower() == login.ToLower());

                if (user == null)
                {
                    throw new NamazException(ErrorCodes.UserErrors.NotExists);
                }

                var hashedPass = HashPassword(user.Salt, password);

                if (hashedPass != user.Password)
                {
                    throw new NamazException(ErrorCodes.UserErrors.PasswordIsInvalid);
                }

                return ResultDto.Succeded(new {user.Id, user.Name});
            }
        }

        public User Get(UserDto dto)
        {
            using (var db = new NamazDbContext())
            {
                return GetUserByEmail(dto, db) ?? GetUserByToken(dto, db);
            }   
        }

        public User GetByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return null;

            using (var db = new NamazDbContext())
            {
                return db.Users.Include(x => x.Schedule).SingleOrDefault(x => x.Email.ToLower() == email.ToLower());
            }
        }

        public ResultDto Create(UserDto userInfo)
        {
            if (userInfo == null)
                throw new NamazException(ErrorCodes.UserErrors.ParametersAreEmpty);

            if (!string.IsNullOrEmpty(userInfo.Token))
            {
                return CreateUserViaToken(userInfo);
            }

            if (string.IsNullOrEmpty(userInfo.Name))
                throw new NamazException(ErrorCodes.UserErrors.NameIsEmpty);
            if (string.IsNullOrEmpty(userInfo.Email))
                throw new NamazException(ErrorCodes.UserErrors.EmailIsEmpty);
            if (string.IsNullOrEmpty(userInfo.Password))
                throw new NamazException(ErrorCodes.UserErrors.PasswordIsEmpty);

            if (Exists(userInfo.Email))
                throw new NamazException(ErrorCodes.UserErrors.Exists);

            string salt = CreateSalt();
            string password = HashPassword(salt, userInfo.Password);
            long id = 0;

            try
            {
                using (var db = new NamazDbContext())
                {
                    var user = new User
                    {
                        Name = userInfo.Name,
                        Email = userInfo.Email,
                        Password = password,
                        Salt = salt
                    };
                    db.Users.Add(user);

                    db.SaveChanges();

                    id = user.Id;
                }
            }
            catch (Exception ex)
            {
                throw new NamazException(ErrorCodes.UserErrors.Unknown);
            }

            return new ResultDto() {Data = id, Success = true};
        }

        public ResultDto ChangeSmth(UserDto userInfo, string field, string newVal)
        {
            if (userInfo == null || string.IsNullOrEmpty(field) || string.IsNullOrEmpty(newVal))
                throw new NamazException(ErrorCodes.UserErrors.ParametersAreEmpty);

            using (var db = new NamazDbContext())
            {
                var user = db.Users.SingleOrDefault(x => x.Email.ToLower() == userInfo.Email.ToLower());

                if (user == null)
                    throw new NamazException(ErrorCodes.UserErrors.NotExists);

                var hashedPass = HashPassword(user.Salt, userInfo.Password);

                if (hashedPass != user.Password)
                    throw new NamazException(ErrorCodes.UserErrors.PasswordIsInvalid);

                field = field.ToLowerInvariant();
                
                if (field == "email")
                    return ChangeEmail(db, user, newVal);
                if (field == "password")
                    return ChangePassword(db, user, newVal);
                
                var prop = typeof (User).GetProperties().FirstOrDefault(x => x.Name.ToLowerInvariant() == field);

                if (prop == null)
                    throw new NamazException(ErrorCodes.UserErrors.FieldNotFound);

                var accessor = ObjectAccessor.Create(user);

                accessor[prop.Name] = newVal;

                db.Entry(user).State = EntityState.Modified;

                db.SaveChanges();
            }

            return ResultDto.Succeded();
        }

        public string HashPassword(string salt, string password)
        {
            var hasher = SHA512.Create();

            var bytes = hasher.ComputeHash(Encoding.UTF8.GetBytes(string.Concat(password, salt)));

            var sBuilder = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
            {
                sBuilder.Append(bytes[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        #region private methods

        public string CreateSalt()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        private ResultDto ChangePassword(NamazDbContext db, User user, string password)
        {
            if (string.IsNullOrEmpty(password))
                throw new NamazException(ErrorCodes.UserErrors.PasswordIsEmpty);

            user.Password = HashPassword(user.Salt, password);

            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            return ResultDto.Succeded();
        }

        private ResultDto ChangeEmail(NamazDbContext db, User user, string email)
        {
            if (db.Users.Any(x => x.Email == email))
                throw new NamazException(ErrorCodes.UserErrors.Exists);

            user.Email = email;

            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();

            return ResultDto.Succeded();
        }

        private ResultDto CreateUserViaToken(UserDto userInfo)
        {
            var client = new FacebookClient(userInfo.Token);
            User user = null;

            try
            {
                dynamic me = client.Get("me");

                user = GetUserByFacebookId(me);

                if (user == null)
                {
                    user = new User()
                    {
                        Name = me.name,
                        Email = me.email
                    };

                    var auth = new AuthProvider()
                    {
                        CurrentToken = userInfo.Token,
                        User = user,
                        LastCheckDate = DateTime.Now,
                        ProviderCode = "facebook",
                        ExtId = me.id.ToString()
                    };

                    using (var db = new NamazDbContext())
                    {
                        db.Users.Add(user);
                        db.AuthProviders.Add(auth);

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                throw new NamazException(ErrorCodes.UserErrors.TokenIsInvalid);
            }

            return new ResultDto()
            {
                Data = new
                {
                    id = user.Id,
                    userName = user.Name
                },
                Success = true
            };
        }

        private User GetUserByFacebookId(dynamic me)
        {
            using (var db = new NamazDbContext())
            {
                string id = me.id.ToString();

                return
                    db.AuthProviders.Include(x => x.User)
                        .FirstOrDefault(x => x.ProviderCode == "facebook" && x.ExtId == id)?.User;
            }
        }

        #endregion

        public ResultDto ResetPassword(string email, string locale)
        {
            var subject = "Сброс пароля";
            var text = "Временно установлен пароль";

            if (locale != null && locale?.ToLower() == "en")
            {
                subject = "Password reset";
                text = "Temporary password";
            }

            using (var db = new NamazDbContext())
            {
                var user = db.Users.SingleOrDefault(x => x.Email.ToLower() == email.ToLower());

                if (user == null)
                    throw new NamazException(ErrorCodes.UserErrors.NotExists);

                var pass = GetRandomString();

                ChangePassword(db, user, pass);

                var msg = new MailMessage("salattime@teorius.ru", email, subject, 
                    string.Format("{0}: {1}", text, pass));

                var client = new SmtpClient("smtp.timeweb.ru");
                client.Credentials = new NetworkCredential("salattime@teorius.ru", "SalatTime123");
                client.Port = 25;
                client.EnableSsl = false;
                client.Send(msg);
                
                db.SaveChanges();
            }

            return ResultDto.Succeded();
        }

        private  string GetRandomString()
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            return path;
        }

        #region get user helpers

        private User GetUserByToken(UserDto userDto, NamazDbContext db, string provider = "facebook")
        {
            if (string.IsNullOrEmpty(userDto.Token)) return null;

            var auth = db.AuthProviders
                .Include(x => x.User)
                .ThenInclude(x => x.Schedule)
                .SingleOrDefault(x => x.ProviderCode == provider && x.CurrentToken == userDto.Token);

            if (auth == null)
            {
                var client = new FacebookClient(userDto.Token);

                try
                {
                    dynamic me = client.Get("me");

                    string id = me.id.ToString();

                    auth =
                        db.AuthProviders.Include(x => x.User)
                            .FirstOrDefault(x => x.ProviderCode == "facebook" && x.ExtId == id);
                }
                catch
                {
                    return null;
                }
            }

            if (auth == null)
                return null;

            if (DateTime.Now.Date > auth.LastCheckDate.AddDays(_tokenExp).Date)
            {
                var client = new FacebookClient(auth.CurrentToken);

                try
                {
                    client.Get("me");
                }
                catch
                {
                    throw new NamazException(ErrorCodes.UserErrors.TokenHasExpired);
                }
            }

            auth.CurrentToken = userDto.Token;
            auth.LastCheckDate = DateTime.Now.Date;
            db.Entry(auth).State = EntityState.Modified;

            return auth.User;
        }

        private User GetUserByEmail(UserDto userDto, NamazDbContext db)
        {
            var userFromDb = GetByEmail(userDto.Email);

            if (userFromDb == null)
                return null;

            string password = HashPassword(userFromDb.Salt, userDto.Password);

            var user = db.Users.FirstOrDefault(x => x.Email == userDto.Email && x.Password == password);

            return user;
        }

        #endregion
    }
}