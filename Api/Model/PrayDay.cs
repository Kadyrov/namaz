﻿namespace Api.Model
{
    public class PrayDay
    {
        public int DayOfMonth { get; set; }

        public PrayTime Fadzhr { get; set; }
        public PrayTime Voshod { get; set; }
        public PrayTime Zuhr { get; set; }
        public PrayTime Asr { get; set; }
        public PrayTime Magrib { get; set; }
        public PrayTime Isha { get; set; }
    }
}