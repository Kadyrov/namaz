﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Api.Model
{
    [Table("city")]
    public class City : BaseEntity
    {
        [JsonProperty("name_en"), Column("name_en")]
        public string NameEn { get; set; }
        
        [JsonProperty("id"), Column("ext_id")]
        public string ExtId { get; set; }

        [JsonProperty("country_id"), Column("country_ext_id")]
        public string CountryExtId { get; set; }
        
        [JsonProperty("lat"), Column("lat")]
        public string Lat { get; set; }
        
        [JsonProperty("lng"), Column("lng")]
        public string Lng { get; set; }

        [JsonProperty("region_id"), Column("region_ext_id")]
        public string RegionExtId { get; set; }
        
        [Column("id"), Key, JsonIgnore]
        public override long Id { get; set; }

        [Column("holder_id"), ForeignKey(nameof(Holder))]
        public long HolderId { get; set; }
        
        public CitiesHolder Holder { get; set; }
    }
}