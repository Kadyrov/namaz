﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Api.Model
{
    public class Schedule : BaseEntity, ISchedule
    {
        [Column("next_id"), ForeignKey(nameof(Next))]
        public long? NextId { get; set; }
       
        public Schedule Next { get; set; }

        [Column("month")]
        public int Month { get; set; }

        [Column("city_id"), ForeignKey(nameof(City))]
        public long CityId { get; set; }
        
        public City City { get; set; }

        [Column("counter")]
        public int Counter { get; set; }

        [Column("hash")]
        public string Hash { get; set; }

        [Column("user_id"), ForeignKey(nameof(Creator))]
        public long CreatorId { get; set; }
        public User Creator { get; set; }

        [Column("source")]
        public string Source { get; set; }

        [Column("year")]
        public int Year { get; set; }
        
        [Column("days")]
        [JsonIgnore]
        public byte[] DaysAsBlob
        {
            get
            {
                if (_days != null && _days.Any())
                    return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(_days));
                return new byte[0];
            }
            set
            {
                _blobData = value;
                if (_blobData != null)
                    _days = JsonConvert.DeserializeObject<List<PrayDay>>(Encoding.UTF8.GetString(_blobData));
                else
                    _days = new List<PrayDay>();
            }
        }

        [NotMapped]
        public List<PrayDay> Days {
            get { return _days; }
            set
            {
                _days = value;
                if (value != null)
                    _blobData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(_days));
            }
        }

        public Schedule()
        {
            Days = new List<PrayDay>();
        }

        private byte[] _blobData;
        private List<PrayDay> _days;
    }
}