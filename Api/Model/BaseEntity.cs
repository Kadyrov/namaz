﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Model
{
    public class BaseEntity : IHaveId
    {
        [Column("id"), Key]
        public virtual long Id { get; set; }

        [Column("date_create")]
        public virtual DateTime CreateDate { get; set; }

        [Column("date_edit")]
        public virtual DateTime EditDate { get; set; }
    }
}