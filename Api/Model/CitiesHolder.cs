﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Api.Model
{
    [Table("cities_holder")]
    public class CitiesHolder : BaseEntity
    {
        [JsonProperty("cities")]
        public ICollection<City> Cities { get; set; }

        [JsonProperty("regions"), NotMapped]
        public ICollection<Region> Regions { get; set; }

        [JsonProperty("time_zone"), Column("tz")]
        public string TimeZone { get; set; }
        
        [JsonProperty("name_en"), Column("name_en")]
        public string NameEn { get; set; }
        
        [JsonProperty("id"), Column("ext_id")]
        public string ExtId { get; set; }

        [JsonProperty("name_ru"), Column("name_ru")]
        public string NameRu { get; set; }
    }

    public class Region
    {
        [JsonProperty("cities"), NotMapped]
        public ICollection<City> Cities { get; set; }

        [JsonProperty("id"), NotMapped]
        public string Id { get; set; }
    }
}