﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Model
{
    public class User : BaseEntity
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("password")]
        public string Password { get; set; }

        [Column("salt")]
        public string Salt { get; set; }

        [Column("schedule_id"), ForeignKey(nameof(Schedule))]
        public long? ScheduleId { get; set; }

        public Schedule Schedule { get; set; }
    }
}