﻿namespace Api.Model
{
    public interface IHaveId
    {
        long Id { get; set; }
    }
}