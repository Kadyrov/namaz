﻿using Microsoft.AspNetCore.Mvc;

namespace Api.Model.Dto
{
    [ModelBinder(typeof(JsonModelBinder))]
    public class UserDto
    {
        private string _name;
        private string _email;

        public string Name
        {
            get { return _name; }
            set { _name = value?.Trim(); }
        }

        public string Password { get; set; }

        public string Email
        {
            get { return _email; }
            set { _email = value?.Trim(); }
        }

        public string Token { get; set; }
    }
}