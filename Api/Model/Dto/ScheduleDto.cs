﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Api.Model.Dto
{
    [ModelBinder(typeof(JsonModelBinder))]
    public class ScheduleDto : ISchedule
    {
        public ScheduleDto()
        {
            Days = new List<PrayDay>();
        }

        public long Id { get; set; }

        public string Source { get; set; }

        public int Counter { get; set; }

        public int Month { get; set; }

        public int Year { get; set; }

        public string CityId { get; set; }

        public string AuthorName { get; set; }

        public List<PrayDay> Days { get; set; }
    }
}