﻿using System.Collections.Generic;

namespace Api.Model
{
    public interface ISchedule
    {
        int Month { get; set; }

        List<PrayDay> Days { get; set; }
    }
}